<?php

use Cake\Core\Configure;

Configure::write('PRINT_DEBUG', filter_var(getenv('PRINT_DEBUG'), FILTER_VALIDATE_BOOLEAN));

Configure::write('SESSION_DEBUG', filter_var(getenv('SESSION_DEBUG'), FILTER_VALIDATE_BOOLEAN));

/** @var \App\Application $Application */
$Application = new \App\Application(CONFIG);

$Application->addPlugin('ApiHandler');

$Application->addPlugin('Languages', ['routes' => true]);

$Application->addPlugin('AuthApi', ['routes' => true]);

$Application->addPlugin('Seeds');

$Application->addPlugin('RemoveContents');


$Application->addPlugin('UserActivities');

$UserActivitiesListener = new \UserActivities\EventListener\UserAcitivitiesListener();
\Cake\Event\EventManager::instance()->on($UserActivitiesListener);


if (!defined('CORE_CONFIG')) {
    define('CORE_CONFIG', __DIR__ . DS);
}
