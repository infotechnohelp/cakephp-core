<?php

use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;
use Cake\Core\Configure;

Router::defaultRouteClass(DashedRoute::class);

$routerCallback = function (RouteBuilder $routes) {

    $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
        'httpOnly' => true
    ]));

    $routes->applyMiddleware('csrf');
    $routes->fallbacks(DashedRoute::class);
};

Router::scope('/', $routerCallback);

$routerCallbacks = Configure::read('routerCallbacks');

if (empty($routerCallbacks)) {
    $routerCallbacks = [];
}

Configure::write('routerCallbacks', array_merge($routerCallbacks, ['cakephp-core' => $routerCallback]));
