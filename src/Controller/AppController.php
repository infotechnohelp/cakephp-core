<?php

declare(strict_types=1);

namespace Core\Controller;

use \Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\I18n\I18n;
use Languages\Lib\LocaleManager;

/**
 * Class AppController
 * @package Core\Controller
 */
class AppController extends Controller
{
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Cookie');
        $this->set('cookieHelper', $this->Cookie);

        $this->loadComponent('Languages.Languages');

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);

        $this->loadComponent('AuthApi.Authentication');

        $this->loadComponent('UserActivities.LogRequests');

        if (Configure::read('PRINT_DEBUG')) {
            debug('CoreController');
            debug('Implemented languages are: ' .
                json_encode(LocaleManager::getLanguageLabels(['en_US' => 'US'])));
        }

        if (Configure::read('SESSION_DEBUG')) {
            $this->getRequest()->getSession()->write('SESSION_DEBUG', 'CoreController;');
        }
    }

    public function beforeFilter(Event $event)
    {
        if (Configure::read('PRINT_DEBUG')) {
            debug('Cookie language: ' . $this->Cookie->read('language'));
            debug('CakePHP locale language: ' . I18n::getLocale());
        }
    }
}
