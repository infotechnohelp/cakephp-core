<?= $this->Html->meta('icon') ?>

<?php
if (\Cake\Core\Configure::read('SESSION_DEBUG')) {
    $this->getRequest()->getSession()->write('SESSION_DEBUG', sprintf('%s%s',
        $this->getRequest()->getSession()->read('SESSION_DEBUG'), 'CoreDefaultLayout;'));
}
?>

<?php

use PackageLoader\PackageLoader;

$npm = new PackageLoader(\Cake\Routing\Router::url('/') . 'node_modules/', CORE_CONFIG . 'npm-map');

echo $npm->map()->scripts();


// @todo This hack should not be required
if (getenv('DEBUG_JS') === false) {
    $env = new \Dotenv\Dotenv(CONFIG);
    $env->overload();
}

?>

<script>
    const api = new Api("<?= \Cake\Routing\Router::url('/', true); ?>", "api/",
        "<?= $this->request->getParam('_csrfToken'); ?>");

    if (api instanceof Api === false) {
        throw Error('constant `api` is not an instance of `Api`');
    }
</script>

<?= \Languages\Lib\PhrasesLoader::jsPath() ?>

<script>
    const locale = new Locale();

    if (locale instanceof Locale === false) {
        throw Error('constant `locale` is not an instance of `Locale`');
    }
</script>

<script>
    const DEBUG_JS = <?= getenv('DEBUG_JS')?>;

    const log = new Log(DEBUG_JS, true, true);
</script>


<?php if(filter_var(getenv('PRINT_DEBUG'), FILTER_VALIDATE_BOOLEAN)){ ?>
    <h3>Core default layout, HTML log implemented</h3>
<?php } ?>

<?= $this->fetch('content') ?>

